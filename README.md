# jenkinsfiles

Ce repository contient les jenkinsfiles des jobs jenkins du projet Collector+ :

- `terraform.groovy` : [deploy_infrastructure](https://jenkins-pic-mat.apps.eul.sncf.fr/job/groupehv/job/03151/job/deploy_infrastructure/)
- `build_collector.groovy` : [build_front](https://jenkins-pic-mat.apps.eul.sncf.fr/job/groupehv/job/03151/job/build_front/)
- `build_docker_images.groovy` : [build_docker_images](https://jenkins-pic-mat.apps.eul.sncf.fr/job/groupehv/job/03151/job/build_docker_images/)
- `ansible.groovy` : [03151_deploy_hprod](https://jenkins-pic-mat.apps.eul.sncf.fr/job/groupehv/job/03151/job/03151_deploy_hprod/)
- `build_and_deploy_collector.groovy` : [build_and_deploy_collector](https://jenkins-pic-mat.apps.eul.sncf.fr/job/groupehv/job/03151/job/build_and_deploy_collector/)
- `restore_bdd_dump.groovy` : [import_bdd_dump](https://jenkins-pic-mat.apps.eul.sncf.fr/job/groupehv/job/03151/job/import_bdd_dump/)

<img src="img/automatisation_global.jpg">