// Doc : https://gitlab-repo-gpf.apps.eul.sncf.fr/dosn/2608/documentation/documentations/blob/master/general/deploy.md#param%C3%A8tres
@Library("deployment") _

def p_version                = params.version
def p_reference_git_playbook = params.branche_ou_tag
def p_env                    = params.environnement
def p_extra                  = "version: ${p_version}"
def p_vault                  = params.vault
def p_playbook               = params.playbook
def micro_service

switch(p_playbook) {
    case "playbook_front.yml":
        micro_service = "front + api + batch"
        break
    case "playbook_cronjobs.yml":
        micro_service = "cronjobs"
        break
    case "playbook_filebeat.yml":
        micro_service = "filebeat"
        break
    default:
        micro_service = "unknown"
        break
}

currentBuild.displayName = "#${BUILD_NUMBER} - ${p_env} - ${micro_service} - ${p_version}"
currentBuild.description = "Déploiement de la version ${p_version} du MS ${micro_service} sur ${p_env}"

deploy {
    idAresis                 = "03151"
    nomAresis                = "CollectorPlus"
    playbookName             = p_playbook
    versionAnsible           = "5"
    urlPlaybook              = "https://gitlab-repo-mob.apps.eul.sncf.fr/dsit-mat/groupehv/03151/deploy_collector.git"
    gitlab_credential        = "gitlab"
    reference_git_playbook   = p_reference_git_playbook
    env                      = p_env
    extra                    = p_extra
    verbosity                = "vv"
    vault                    = p_vault
    version                  = p_version
    checkString              = false
    rollback                 = false
}
