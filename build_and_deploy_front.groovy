#!/usr/bin/env groovy

// Version templatefile node : 2.0.0

// Déclaration de la shared library build_libs

@Library('build_libs') _

def environment
def branche_or_tag
def branch_deploy
def version
def tag

//Webhook déclenché par un push sur https://gitlab-repo-mob.apps.eul.sncf.fr/dsit-mat/groupehv/03151/FrontPHP.git
if (env.gitlabActionType == "PUSH") {
    currentBuild.displayName = "#${BUILD_NUMBER} - ${env.gitlabTargetBranch}"
    if (env.gitlabTargetBranch.split("-")[0].contains("dev0")) {
        environment = env.gitlabTargetBranch.split("-")[0] //env.gitlabTargetBranch est au format dev0x-*
        branch_deploy = "develop"
    }
    else if (env.gitlabTargetBranch == "develop") {
        environment = "integration"
        branch_deploy = "develop"
    }
    else if (env.gitlabTargetBranch == "release") {
        environment = "recette"
        branch_deploy = "release"
    }
    else {
        currentBuild.result = 'ABORTED'
        error("Le job a été déclenché par un push fictif (test de webhook par exemple).")
    }
    branche_or_tag = env.gitlabTargetBranch
    version = "${environment}-${BUILD_NUMBER}-SNAPSHOT"
}
//Webhook déclenché par un tag sur https://gitlab-repo-mob.apps.eul.sncf.fr/dsit-mat/groupehv/03151/FrontPHP.git
else if (env.gitlabActionType == "TAG_PUSH") {
    tag = env.gitlabTargetBranch.split("/")[2] //env.gitlabTargetBranch est au format refs/tags/valeur-du-tag
    currentBuild.displayName = "#${BUILD_NUMBER} - ${tag}"
    //vérification du format du tag
    if (tag.matches(/(\d+)\.(\d+)\.(\d+)/)) {
        environment = "pre-prod"
        branch_deploy = "master"
        version = "${tag}"
    }
    else {
        currentBuild.result = 'ABORTED'
        error("Le tag ${tag} n'est pas au format X.Y.Z avec X, Y et Z étant des nombres d'au moins 1 digit.")
    }
    branche_or_tag = "${tag}"
}
else {
    currentBuild.result = 'ABORTED'
    error("Le job doit obligatoirement être déclenché par un webhook du repository https://gitlab-repo-mob.apps.eul.sncf.fr/dsit-mat/groupehv/03151/FrontPHP.git")
}

timestamps {
    node('jenkins-slave') {
        stage('Build front Artifact_and_Docker_Image') {
            build(
                job: "groupehv/03151/migration_artifactory/build_test_front",
                parameters: [
                    string(name: 'branche_ou_tag', value: "${branche_or_tag}"),
                    string(name: 'version', value: "${version}")
                ]
            )
        }
        stage('Build API Artifact_and_Docker_Image') {
            build(
                job: "groupehv/03151/migration_artifactory/build_test_api",
                parameters: [
                    string(name: 'branche_ou_tag', value: "${branche_or_tag}"),
                    string(name: 'version', value: "${version}")
                ]
            )
        }
        stage('Build Batch Artifact_and_Docker_Image') {
            build(
                job: "groupehv/03151/migration_artifactory/build_test_batch",
                parameters: [
                    string(name: 'branche_ou_tag', value: "${branche_or_tag}"),
                    string(name: 'version', value: "${version}")
                ]
            )
        }
        stage('Deploy') {
            build(
                job: "groupehv/03151/migration_artifactory/03151_deploy_hprod",
                parameters: [
                    [$class: 'StringParameterValue', name: 'branche_ou_tag', value: "${branch_deploy}"],
                    [$class: 'StringParameterValue', name: 'environnement', value: "${environment}"],
                    [$class: 'StringParameterValue', name: 'version', value: "${version}"],
                    [$class: 'StringParameterValue', name: 'playbook', value: "playbook_front.yml"]
                ]
            )
        }
    }
}
