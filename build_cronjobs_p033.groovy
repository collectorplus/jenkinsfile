#!/usr/bin/env groovy

// Version templatefile node : 2.0.0

// Déclaration de la shared library build_libs

@Library('build_libs') _

def branch_dockerfile_name = "master"
def branche_or_tag = params.branche_ou_tag
def dev_or_prod
def image_name = "cronjobs-p033" //Le nom de l'image docker doit être OBLIGATOIREMENT en minuscule & correspondre au nom de l'artefact
def options = defaultBuildOptions()
def package_type = "maven"
def url_code_source = "https://gitlab-repo-mob.apps.eul.sncf.fr/dsit-mat/groupehv/03151/flux.git"
def url_dockerfile = "https://gitlab-repo-mob.apps.eul.sncf.fr/dsit-mat/groupehv/03151/dockerfile_cronjobs.git"
def version = params.version

currentBuild.displayName = "#${BUILD_NUMBER} - ${version}"

// Version au format xxx-SNAPSHOT
if (version.split("-").length == 2) {
    if (version.split("-")[1] == "SNAPSHOT") {
        dev_or_prod = "dev"
    }
    else {
        currentBuild.result = 'ABORTED'
        error("La version doit être au format xxx-SNAPSHOT ou X.Y.Z avec X, Y et Z étant des nombres d'au moins 1 digit. Version fournie : ${version}.")
    }
}

// Version au format X.Y.Z
if (version.split("-").length == 1) {
    if (version.matches(/(\d+)\.(\d+)\.(\d+)/)) {
        dev_or_prod = "prod"
    }
    else {
        currentBuild.result = 'ABORTED'
        error("Le tag ${version} n'est pas au format X.Y.Z avec X, Y et Z étant des nombres d'au moins 1 digit.")
    }
}

timestamps {
    withTools([
        [name: 'node', image: 'node-web-browsers', registry: 'eul', version: 'erbium'],
        [name: 'sonar-scanner', version: 'latest', registry: 'eul'],
        [name: 'buildkit', image: 'moby/buildkit', version: 'v0.10.3-rootless'],
    ]){
        stage('Récupération code source') {
            println '🔰 Récupération du code source'
            scmInfo = checkout([
                $class: 'GitSCM', 
                branches: [[name: "${branche_or_tag}"]], 
                userRemoteConfigs: [[credentialsId: "gitlab", url: "${url_code_source}"]]
            ])
        }
        stage('Zip du code source') {
            sh "tar -cvf ${image_name}.tar --exclude=.git* --exclude=README.md *"
            sh "gzip ${image_name}.tar"
        }
        // stage('Tests') {
        //     container('node') {
        //         println '🔰 Exécution des tests unitaires'
        //         if (sh(script: 'eul npm test', returnStatus: true)) {
        //             unstable '⚠️️ Les tests unitaires ont échoué'
        //         }
        //         println '✔️️ Exécution des tests unitaires effectuée'
        //     }
        // }
        // stage('QA') {
        //     container('sonar-scanner') {
        //         println '🔰 Analyse qualité'
        //             // Le nom de la branche est ajouté à la fin du nom du projet SonarQube
        //             String sonarBranch = "${branche_or_tag}"
        //             String sonarProjectName = "${branche_or_tag} ${sonarBranch}".trim()
        //             sh """\
        //                 eul sonar-scanner \
        //                 -Dsonar.projectKey="sncf.mobilitie.groupehv.03151:${image_name}:develop" \
        //                 -Dsonar.projectName="${image_name}" \
        //                 -Dsonar.projectVersion=${env.VERSION} \
        //                 -Dsonar.links.ci=${JOB_URL} \
        //                 -Dsonar.links.homepage=${GIT_URL} \
        //             """
        //         println '✔️️ Analyse qualité effectuée'
        //     }
        // }
        // stage('Local CVE scan') {
        //     container('node') {
        //         println '🔰 Scan de sécurité local sur les artefacts produits'
        //         // Scan CVE déclenché avant la publication des artefacts pour ne pas publier d'artefact avec des vulnerabilités
        //         if (sh(script: 'eul artefacts cve-scan --local --critical --npm', returnStatus: true)) {
        //             // La présence de cve dans les artefacts est bloquante
        //             error "❌ Le scan de CVE a identifié des vulnérabilités critiques dans l'artefact ou ses dépendances"
        //         }
        //         println '✔️️ Scan CVE local effectué'
        //     }
        // }
        stage('Publish') {
            container('node') {
                println "🔰 Publication des artefacts du projet dans Artifactory"
                // En convention maven
                String groupId = "sncf/mobilite/groupehv/collectorplus"
                String targetLocation = "$groupId/${image_name}/${version}/${image_name}-${version}.tar.gz"
                // Upload au format generic avec une convention maven.
                // Avec le generic, on est libre sur le choix de la convention et donc du dossier de destination :
                // eul artefact upload -T generic $archiveFilename a/b/c/d/e/"
                // sans paramètre supplémentaire le dépôt de destination par défaut est packageType_areris_dev
                if (sh(script: "eul artefacts --repo-target ${dev_or_prod} upload -T ${package_type} ${image_name}.tar.gz $targetLocation", returnStatus: true)) {
                    error "❌ L'upload de l'artefact ${image_name}.tar.gz a échoué"
                }
                // Publication des infos de build dans artifactory
                sh 'eul artefacts build-publish'
                // Publication Jenkins du lien sur le build Artifactory
                publishArtifactoryBuildLink()
                println '✔️ Publication des artefacts effectuée'
            }
        } 
        // stage('CVE scan') {
        //     container('node') {
        //         println '🔰 Scan CVE distant sur les artefacts produits sur Artifactory'
        //         if (sh(script: 'eul artefacts cve-scan', returnStatus: true)) {
        //             // Si le pipeline arrive à ce stage cela veut dire qu'il a passé le scan local et ne présente pas de CVE critiques mais hautes.
        //             // Pour mettre la pipeline en erreur, remplacer unstable "⚠️..." par error "❌..." dans la ligne suivante
        //             unstable "⚠️️ Le scan de CVE a identifié des vulnérabilités hautes dans l'artefact ou ses dépendances"
        //         }
        //         println '✔️ Scan CVE distant effectué'
        //     }
        // }
        stage('Docker build') {
            container('buildkit') {
                println '🔰 build Docker avec Buildkit'
                String artefact_repo_url = "https://artefact-repo.apps.eul.sncf.fr/artifactory/${package_type}_${env.EUL_ARESIS}_${dev_or_prod}"
                scmInfo = checkout([
                    $class: 'GitSCM', 
                    branches: [[name: "${branch_dockerfile_name}"]], 
                    userRemoteConfigs: [[credentialsId: "gitlab", url: "${url_dockerfile}"]]])
                env.GIT_URL =  scmInfo.GIT_URL
                env.GIT_COMMIT = scmInfo.GIT_COMMIT
                result = sh(script: "eul image --repo-target ${dev_or_prod} --debug build docker --tag=${image_name}:${version} --build-arg 'ARTEFACT_REPO_URL=${artefact_repo_url}' --build-arg 'IMAGE_NAME=${image_name}' --build-arg 'VERSION=${version}'", returnStatus: true)
                if (result != 0) {
                    error '❌ Le build Docker avec BuildKit a échoué'
                }
                println '✔️ build Docker effectué'
            }
        }
// Voir https://jenkins.io/doc/pipeline/steps/gitlab-plugin/ pour plus de précisions
updateGitlabCommitStatus name: 'build', state: 'success'
println '👍 Build du job de snapshot terminé avec succès'
currentBuild.result = 'SUCCESS'
    }
}
