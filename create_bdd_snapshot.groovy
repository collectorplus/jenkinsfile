#!/usr/bin/env groovy
//Version template jenkinsfile iac : 0.1.0
@Library(['iac_libs']) _

//Comptes et rôles AWS pour chaque environnement
Map environnements = [
    "dev01" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00003'
    ],
    "dev02" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00004'
    ],
    "dev03" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00005'
    ],
    "dev04" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00006'
    ],
    "dev05" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00007'
    ],
    "dev06" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00008'
    ],
    "dev07" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00009'
    ],
    "dev08" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00010'
    ],
    "dev09" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00011'
    ],
    "integration" : [
        roleAws : 'col-nonprod-iac-11',
        accountAws: '406190920048',
        rdsIdentifier: 'pabdcoli00001'
    ],
    "recette" : [
        roleAws : 'col-nonprod-iac-11',
        accountAws: '406190920048',
        rdsIdentifier: 'pabdcolr00001'
    ],
    "pre-prod" : [
        roleAws : 'col-nonprod-iac-11',
        accountAws: '406190920048',
        rdsIdentifier: 'pabdcolh00002'
    ],
    "formation" : [
        roleAws : 'col-prod-iac-11',
        accountAws: '471911455973',
        rdsIdentifier: 'pabdcolf00001'
    ],
    "production" : [
        roleAws : 'col-prod-iac-11',
        accountAws: '471911455973',
        rdsIdentifier: 'pabdcolp00002'
    ]
]

def environnement_p = params.Environnement
def awsRole = environnements[environnement_p]['roleAws']
def awsAccountId = environnements[environnement_p]['accountAws']
def snapshot_identifier = params.snapshot_identifier
def new_db_identifier = params.new_db_identifier
def rds_identifier = environnements[environnement_p]['rdsIdentifier']

currentBuild.displayName = "#${BUILD_NUMBER} - ${environnement_p}"
currentBuild.description = "Creation du snpashot ${snapshot_identifier} de l'instance  ${rds_identifier}"

iacWrapper([terraformVersion: "0.14-full", logLevel: "ERROR"]) {
    withEnv(["TF_VAR_aws_account_id=${awsAccountId}", "TF_VAR_aws_role=${awsRole}"]) {
        container('terraform') {
            stage("Initialisation des credentials AWS") {
                awsLoadCredentials(awsAccountId, awsRole)
            }
            stage("Création du profile deploy_account") {
                sh "touch ~/.aws/config"
                sh 'echo "[profile deploy_account]" > ~/.aws/config'
                sh "echo \"role_arn = arn:aws:iam::${awsAccountId}:role/${awsRole}\" >> ~/.aws/config"
                sh 'echo "source_profile = saml" >> ~/.aws/config'
                sh 'echo "region=eu-west-3" >> ~/.aws/config'
                sh "cat ~/.aws/config"
            }
            stage("Creation du snapshot ${snapshot_identifier} de l'instance ${rds_identifier}") {
                sh "aws rds create-db-snapshot --db-instance-identifier ${rds_identifier} --db-snapshot-identifier ${snapshot_identifier} --profile deploy_account"
            }
            stage("Description du snapshot ${snapshot_identifier}") {
                sh "aws rds describe-db-snapshots --db-snapshot-identifier ${snapshot_identifier} --profile deploy_account"
            }
            stage("Sleep for 5 mn to delete ${rds_identifier}") {
                sh "sleep 360"
            }
        } 
    }
}
