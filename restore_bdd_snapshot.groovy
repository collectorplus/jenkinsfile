#!/usr/bin/env groovy
//Version template jenkinsfile iac : 0.1.0
@Library(['iac_libs']) _

//Comptes et rôles AWS pour chaque environnement
Map environnements = [
    "dev01" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00003',
        dbSubnetGrp: 'pabdcold00003-subnet',
        vpcSgId1: 'sg-01ef355dc23b2e090 ',
        vpcSgId2: 'sg-0eea5028b04199760',
        vpcSgId3: 'sg-0dcd01ab472253493',
        
    ],
    "dev02" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00004',
        dbSubnetGrp: 'pabdcold00004-subnet',
        vpcSgId1: 'sg-01ef355dc23b2e090 ',
        vpcSgId2: 'sg-0eea5028b04199760',
        vpcSgId3: 'sg-0dcd01ab472253493',
     
    ],
    "dev03" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00005',
        dbSubnetGrp: 'pabdcold00005-subnet',
        vpcSgId1: 'sg-01ef355dc23b2e090 ',
        vpcSgId2: 'sg-0eea5028b04199760',
        vpcSgId3: 'sg-0dcd01ab472253493',
        
    ],
    "dev04" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00006',
        dbSubnetGrp: 'pabdcold00006-subnet',
        vpcSgId1: 'sg-01ef355dc23b2e090 ',
        vpcSgId2: 'sg-0eea5028b04199760',
        vpcSgId3: 'sg-0dcd01ab472253493',
        
    ],
    "dev05" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00007',
        dbSubnetGrp: 'pabdcold00007-subnet',
        vpcSgId1: 'sg-01ef355dc23b2e090 ',
        vpcSgId2: 'sg-0eea5028b04199760',
        vpcSgId3: 'sg-0dcd01ab472253493',
      
    ],
    "dev06" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00008',
        dbSubnetGrp: 'pabdcold00008-subnet',
        vpcSgId1: 'sg-01ef355dc23b2e090 ',
        vpcSgId2: 'sg-0eea5028b04199760',
        vpcSgId3: 'sg-0dcd01ab472253493',
        
    ],
    "dev07" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00009',
        dbSubnetGrp: 'pabdcold00009-subnet',
        vpcSgId1: 'sg-01ef355dc23b2e090 ',
        vpcSgId2: 'sg-0eea5028b04199760',
        vpcSgId3: 'sg-0dcd01ab472253493',
    
    ],
    "dev08" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00010',
        dbSubnetGrp: 'pabdcold00010-subnet',
        vpcSgId1: 'sg-01ef355dc23b2e090 ',
        vpcSgId2: 'sg-0eea5028b04199760',
        vpcSgId3: 'sg-0dcd01ab472253493',
        
    ],
    "dev09" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsIdentifier: 'pabdcold00011',
        dbSubnetGrp: 'pabdcold00011-subnet',
        vpcSgId1: 'sg-01ef355dc23b2e090 ',
        vpcSgId2: 'sg-0eea5028b04199760',
        vpcSgId3: 'sg-0dcd01ab472253493',
        
    ],
    "integration" : [
        roleAws : 'col-nonprod-iac-11',
        accountAws: '406190920048',
        rdsIdentifier: 'pabdcoli00001',
        dbSubnetGrp: 'pabdcoli00001-subnet',
        vpcSgId1: 'sg-0ab1192a2fe342e77 ',
        vpcSgId2: 'sg-0b8d4787e5c3a24ac',
        vpcSgId3: 'sg-0db9d790dcdb36ace',
        
    ],
    "recette" : [
        roleAws : 'col-nonprod-iac-11',
        accountAws: '406190920048',
        rdsIdentifier: 'pabdcolr00001',
        dbSubnetGrp: 'pabdcolr00001-subnet',
        vpcSgId1: 'sg-0ab1192a2fe342e77 ',
        vpcSgId2: 'sg-0b8d4787e5c3a24ac',
        vpcSgId3: 'sg-0db9d790dcdb36ace',
        
    ],
    "pre-prod" : [
        roleAws : 'col-nonprod-iac-11',
        accountAws: '406190920048',
        rdsIdentifier: 'pabdcolh00002',
        dbSubnetGrp: 'pabdcolh00002-subnet',
        vpcSgId1: 'sg-0ab1192a2fe342e77 ',
        vpcSgId2: 'sg-0b8d4787e5c3a24ac',
        vpcSgId3: 'sg-0db9d790dcdb36ace',
        
    ],
    "formation" : [
        roleAws : 'col-prod-iac-11',
        accountAws: '471911455973',
        rdsIdentifier: 'pabdcolf00001',
        dbSubnetGrp: 'pabdcolf00001-subnet',
        vpcSgId1: 'sg-02549a2e4d53f4c34 ',
        vpcSgId2: 'sg-055a97d658422795a',
        vpcSgId3: 'sg-0c36bcde54e25df2a',
        
    ],
    "production" : [
        roleAws : 'col-prod-iac-11',
        accountAws: '471911455973',
        rdsIdentifier: 'pabdcolp00002',
        dbSubnetGrp: 'pabdcolp00002-subnet',
        vpcSgId1: 'sg-02549a2e4d53f4c34 ',
        vpcSgId2: 'sg-055a97d658422795a',
        vpcSgId3: 'sg-0c36bcde54e25df2a',
        
    ]
]

def environnement_p = params.Environnement
def awsRole = environnements[environnement_p]['roleAws']
def awsAccountId = environnements[environnement_p]['accountAws']
def snapshot_identifier = params.snapshot_identifier
def new_db_identifier = params.new_db_identifier
def rds_identifier = environnements[environnement_p]['rdsIdentifier']
def db_subnet_grp = environnements[environnement_p]['dbSubnetGrp']
def vpc_sg_id1= environnements[environnement_p]['vpcSgId1']
def vpc_sg_id2= environnements[environnement_p]['vpcSgId2']
def vpc_sg_id3= environnements[environnement_p]['vpcSgId3']



currentBuild.displayName = "#${BUILD_NUMBER} - ${environnement_p}"
currentBuild.description = "Restoration du snpashot ${snapshot_identifier} dans l'instance ${new_db_identifier}"

iacWrapper([terraformVersion: "0.14-full", logLevel: "ERROR"]) {
    withEnv(["TF_VAR_aws_account_id=${awsAccountId}", "TF_VAR_aws_role=${awsRole}"]) {
        container('terraform') {
            stage("Initialisation des credentials AWS") {
                awsLoadCredentials(awsAccountId, awsRole)
            }
            stage("Création du profile deploy_account") {
                sh "touch ~/.aws/config"
                sh 'echo "[profile deploy_account]" > ~/.aws/config'
                sh "echo \"role_arn = arn:aws:iam::${awsAccountId}:role/${awsRole}\" >> ~/.aws/config"
                sh 'echo "source_profile = saml" >> ~/.aws/config'
                sh 'echo "region=eu-west-3" >> ~/.aws/config'
                sh "cat ~/.aws/config"
            }
             stage("Suppression de la BD pour la restauration du snapshot") {
                sh "aws rds delete-db-instance --db-instance-identifier ${rds_identifier} --skip-final-snapshot --no-delete-automated-backups --profile deploy_account"
            }
            stage("Attente de 16 mn pour la suppression de la BD") {
                sh "sleep 1020"
            }
            stage("Restauration du snapshot récupérer sur AWS") {
                sh "aws rds restore-db-instance-from-db-snapshot --db-instance-identifier ${rds_identifier} --db-snapshot-identifier ${snapshot_identifier} --port 5433 --db-subnet-group-name ${db_subnet_grp} --vpc-security-group-ids ${vpc_sg_id1} ${vpc_sg_id2} ${vpc_sg_id3} --multi-az --profile deploy_account"
            }
                        
       }
    }
}
