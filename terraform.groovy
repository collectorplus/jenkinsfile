#!/usr/bin/env groovy
//Version template jenkinsfile iac : 0.1.0
@Library(['iac_libs']) _

//Comptes et rôles AWS pour chaque environnement
Map environnements = [
    "dev01" : [roleAws : 'col-sandbox-iac-11', accountAws: '625151442290'],
    "dev02" : [roleAws : 'col-sandbox-iac-11', accountAws: '625151442290'],
    "dev03" : [roleAws : 'col-sandbox-iac-11', accountAws: '625151442290'],
    "dev04" : [roleAws : 'col-sandbox-iac-11', accountAws: '625151442290'],
    "dev05" : [roleAws : 'col-sandbox-iac-11', accountAws: '625151442290'],
    "dev06" : [roleAws : 'col-sandbox-iac-11', accountAws: '625151442290'],
    "dev07" : [roleAws : 'col-sandbox-iac-11', accountAws: '625151442290'],
    "dev08" : [roleAws : 'col-sandbox-iac-11', accountAws: '625151442290'],
    "dev09" : [roleAws : 'col-sandbox-iac-11', accountAws: '625151442290'],
    "integration" : [roleAws : 'col-nonprod-iac-11', accountAws: '406190920048'],
    "recette" : [roleAws : 'col-nonprod-iac-11', accountAws: '406190920048'],
    "pre-prod" : [roleAws : 'col-nonprod-iac-11', accountAws: '406190920048'],
    "formation" : [roleAws : 'col-prod-iac-11', accountAws: '471911455973'],
    "production" : [roleAws : 'col-prod-iac-11', accountAws: '471911455973']
]

String environnement_p = params.Environnement
def awsRole = environnements[environnement_p]['roleAws']
def awsAccountId = environnements[environnement_p]['accountAws']

currentBuild.displayName = "#${BUILD_NUMBER} - ${environnement_p}"

iacWrapper([terraformVersion: "0.14-full", logLevel: "ERROR"]) {
    withEnv(["TF_VAR_aws_account_id=${awsAccountId}", "TF_VAR_aws_role=${awsRole}"]) {
        container('terraform') {
            // Récupération des sources du projet
            stage ("Téléchargement du code terraform"){
                git branch: params.Branche, credentialsId: "gitlab", url: "https://gitlab-repo-mob.apps.eul.sncf.fr/dsit-mat/groupehv/03151/infrastructure.git"
            }

            stage("Initialisation des credentials AWS") {
                awsLoadCredentials(awsAccountId, awsRole)
            }

            stage("Création du profile deploy_account") {
                sh "touch ~/.aws/config"
                sh 'echo "[profile deploy_account]" > ~/.aws/config'
                sh "echo \"role_arn = arn:aws:iam::${awsAccountId}:role/${awsRole}\" >> ~/.aws/config"
                sh 'echo "source_profile = saml" >> ~/.aws/config'
                sh 'echo "region=eu-west-3" >> ~/.aws/config'
                sh "cat ~/.aws/config"
            }

            //GIT_URL est la variable d'environnement qui désigne le repository utilisé en tant que backend
            env.GIT_URL = "https://gitlab-repo-mob.apps.eul.sncf.fr/dsit-mat/groupehv/03151/infrastructure.git"
            iacInit {
                backend        = 'gitlab'
                environnement  = environnement_p
                options        = [:]
            }

            //Validate
            iacValidate {}

            // Plan
            iacPlan {
                environnement = environnement_p
                options       = [:]
            }

            //Compliance
            iacCompliance {}

            //Apply
            if (params.Apply) {
                stage("Confirm creation") {
                    input( message: "Confirmer l'exécution du Plan ?", ok: "Confirm" )
                }
                iacApply {
                    options       = [:]
                }
            }

            //Destroy
            if (params.Destroy) {
                stage("Confirm destruction") {
                    input( message: "Confirmer la DESTRUCTION de toutes les ressources ?", ok: "Confirm" )
                }
                iacDestroy {
                    environnement = environnement_p
                    options       = [:]
                }
            }
        }
    }
}
