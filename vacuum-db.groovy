#!/usr/bin/env groovy
//Version template jenkinsfile iac : 0.1.0
@Library(['iac_libs']) _

//Comptes et rôles AWS pour chaque environnement
Map environnements = [
    "dev01" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsCluster: 'pabdcold00003.c6ffdnx9v375.eu-west-3.rds.amazonaws.com'
    ],
    "dev02" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsCluster: 'pabdcold00004.c6ffdnx9v375.eu-west-3.rds.amazonaws.com'
    ],
    "dev03" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsCluster: 'pabdcold00005.c6ffdnx9v375.eu-west-3.rds.amazonaws.com'
    ],
    "dev04" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsCluster: 'pabdcold00006.c6ffdnx9v375.eu-west-3.rds.amazonaws.com'
    ],
    "dev05" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsCluster: 'pabdcold00007.c6ffdnx9v375.eu-west-3.rds.amazonaws.com'
    ],
    "dev06" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsCluster: 'pabdcold00008.c6ffdnx9v375.eu-west-3.rds.amazonaws.com'
    ],
    "dev07" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsCluster: 'pabdcold00009.c6ffdnx9v375.eu-west-3.rds.amazonaws.com'
    ],
    "dev08" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsCluster: 'pabdcold00010.c6ffdnx9v375.eu-west-3.rds.amazonaws.com'
    ],
    "dev09" : [
        roleAws : 'col-sandbox-iac-11',
        accountAws: '625151442290',
        rdsCluster: 'pabdcold00011.c6ffdnx9v375.eu-west-3.rds.amazonaws.com'
    ],
    "integration" : [
        roleAws : 'col-nonprod-iac-11',
        accountAws: '406190920048',
        rdsCluster: 'pabdcoli00001.cz1jtrnvbxz0.eu-west-3.rds.amazonaws.com'
    ],
    "recette" : [
        roleAws : 'col-nonprod-iac-11',
        accountAws: '406190920048',
        rdsCluster: 'pabdcolr00001.cz1jtrnvbxz0.eu-west-3.rds.amazonaws.com'
    ],
    "pre-prod" : [
        roleAws : 'col-nonprod-iac-11',
        accountAws: '406190920048',
        rdsCluster: 'pabdcolh00002.cz1jtrnvbxz0.eu-west-3.rds.amazonaws.com'
    ],
    "formation" : [
        roleAws : 'col-prod-iac-11',
        accountAws: '471911455973',
        rdsCluster: 'pabdcolf00001.csmx65dxqia6.eu-west-3.rds.amazonaws.com'
    ],
    "production" : [
        roleAws : 'col-prod-iac-11',
        accountAws: '471911455973',
        rdsCluster: 'pabdcolp00002.csmx65dxqia6.eu-west-3.rds.amazonaws.com'
    ]
]

def environnement_p = params.Environnement
def awsRole = environnements[environnement_p]['roleAws']
def awsAccountId = environnements[environnement_p]['accountAws']
def rds_cluster = environnements[environnement_p]['rdsCluster']

currentBuild.displayName = "#${BUILD_NUMBER} - ${environnement_p}"
currentBuild.description = "Vacuum de la DB du ${rds_cluster}"

iacWrapper([terraformVersion: "0.14-full", logLevel: "ERROR"]) {
    withEnv(["TF_VAR_aws_account_id=${awsAccountId}", "TF_VAR_aws_role=${awsRole}"]) {
        container('terraform') {
            stage("Initialisation des credentials AWS") {
                awsLoadCredentials(awsAccountId, awsRole)
            }
            stage("Création du profile deploy_account") {
                sh "touch ~/.aws/config"
                sh 'echo "[profile deploy_account]" > ~/.aws/config'
                sh "echo \"role_arn = arn:aws:iam::${awsAccountId}:role/${awsRole}\" >> ~/.aws/config"
                sh 'echo "source_profile = saml" >> ~/.aws/config'
                sh 'echo "region=eu-west-3" >> ~/.aws/config'
                sh "cat ~/.aws/config"
            }
            stage("Vacuum DB") {
                withCredentials([string(credentialsId: "${environnement_p}-bdd-password", variable: 'bdd_password')]) {
                    withEnv(["PGPASSWORD=${bdd_password}"]) {
                        sh "vacuumdb -z -d bcol001 -h ${rds_cluster} -p 5433  -U u_bcol001_owner -v"
                    }
                }
            }
           
        }
    }
}
